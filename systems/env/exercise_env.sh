#!/usr/bin/env bash

export basepc=$(hostname -s)

if [[ "$SIMMODE" = true ]]; then
    echo "### SIMMODE IS ACTIVE ###"
    export prefixMain="/vol/tiago/robocup-challenge-team3/"
    export prefix="/vol/tiago/melodic-robocup/"

    export laptop=${basepc}
    export robot=${basepc}
    export ROBOT_VERSION="steel"

    #Simulation map/world
    
    export SIMULATION_WORLD="clf"
    export NAVIGATION_MAP="${prefix}/share/tiago_clf_nav/data/clf_map.yaml"
    #export NAVIGATION_MAP="${PATH_TO_MAPS}/clf_map.yaml"
    #export SIMULATION_WORLD="clf"

    export PATH_TO_KBASE_CONFIG="${prefixMain}/share/robocup_data/knowledgeBase/configs/use_challenge_WS1920_db.yaml"
    export PATH_TO_EDIT_KBASE_CONFIG="${prefixMain}/share/robocup_data/knowledgeBase/configs/edit_challenge_WS1920_db.yaml"

    export OBJECT_DETECTION_THRESHOLD="0.5"

else
    echo "### SIMMODE IS NOT ACTIVE ###"
    export prefixMain="/vol/tiago/robocup-challenge-team3/"
    export prefix="/vol/robocup/tiago-clf-moveitmaster/"

    export laptop=${basepc}
    export robot=tiago-47c

    #Real map
    export PATH_TO_MAPS="${prefixMain}/share/robocup_data/maps"
    export NAVIGATION_USE_MAP=true
    export NAVIGATION_MAP="${PATH_TO_MAPS}/challenge.yaml"

    export PATH_TO_KBASE_CONFIG="${prefixMain}/share/robocup_data/knowledgeBase/configs/use_challenge_WS1920_finale_db.yaml"
    export PATH_TO_EDIT_KBASE_CONFIG="${prefixMain}/share/robocup_data/knowledgeBase/configs/edit_challenge_WS1920_finale_db.yaml"

    export alsa_device="plug_tiago_mono"
fi


export PATH="${prefixMain}/bin:$PATH"


#ROS source alias
export setup_suffix="bash" #$(echo $SHELL | cut -d "/" -f3-)
alias source_ros="source ${prefixMain}/setup.${setup_suffix}"

# Path to bonsai config
export PATH_TO_BONSAI_ROBOCUP_CONFIG="${prefixMain}/opt/bonsai_robocup_addons/etc/bonsai_configs"
export PATH_TO_BONSAI_ROBOCUPTASKS_CONFIG="${prefixMain}/opt/robocup-exercise/etc/bonsai_configs"
export PATH_TO_BONSAI_TIAGO_CONFIG="${prefix}/opt/bonsai_tiago_addons/etc/bonsai_configs"

# Path to scxml locations
export PATH_TO_BONSAI_ROBOCUP_SCXML="${prefix}/opt/bonsai_robocup_addons/etc/state_machines"
export PATH_TO_BONSAI_CORE_SCXML="${prefix}/opt/bonsai-scxml_engine/etc/behaviors"
export PATH_TO_BONSAI_ROBOCUPTASKS_SCXML="${prefixMain}/opt/robocup-exercise/etc/state_machines"
export PATH_TO_BONSAI_PEPPER_SCXML="${prefix}/opt/bonsai2-pepper-dist/etc/state_machines"
export PATH_TO_BONSAI_TIAGO_SCXML="${prefix}/opt/bonsai_tiago_addons/etc/state_machines"

# Create mapping variable, used by bonsai2 to resolve "src=" attributes in scxml files
export BONSAI_MAPPING="ROBOCUP=${PATH_TO_BONSAI_ROBOCUP_SCXML} SCXML=${PATH_TO_BONSAI_CORE_SCXML} EXERCISE=${PATH_TO_BONSAI_ROBOCUPTASKS_SCXML} PEPPER=${PATH_TO_BONSAI_PEPPER_SCXML} TIAGO=${PATH_TO_BONSAI_TIAGO_SCXML}"$

#PocketSphinx paths
export PATH_TO_PSA_CONFIG="${prefixMain}/share/SpeechRec/psConfig"
##########

#Pocketsphinx_grammars
export PSA_CONFIG="${prefixMain}/share/SpeechRec/psConfig/exercise/psteckstor.conf"

#Rviz config
export RVIZ_CONFIG="${prefix}/share/tobi_sim/config/tiago.rviz"

# Default KBASE locations
export PATH_TO_MONGOD_CONFIG="${prefixMain}/share/robocup_data/mongod/mongod.conf"

#object
export PATH_TO_CLASSIFIERS="${prefixMain}/share/robocup_data/tensorflow"
export OBJECT_DET_GRAPH="${PATH_TO_CLASSIFIERS}/frozen_inference_graph_final.pb"
export OBJECT_DET_LABELS="${PATH_TO_CLASSIFIERS}/label_map.pbtxt" 
export OBJECT_REC_PATH="${PATH_TO_CLASSIFIERS}/train"
export OBJECT_REC_GRAPH="${OBJECT_REC_PATH}/output_graph.pb"
export OBJECT_REC_LABELS="${OBJECT_REC_PATH}/output_labels.txt"

# Other
export ACTOR_VEL_TOPIC="/Olf/cmd_vel"
