#!/usr/bin/env bash

######## TODO

if [[ -z "${SIMMODE}" ]]; then
    export SIMMODE="true"
fi

export basepc=$(hostname -s)

if [[ "$SIMMODE" = true ]]; then
    echo "### SIMMODE IS ACTIVE ###"
    export prefix="/vol/tiago/melodic-robocup/"
    export laptop=${basepc}
    export robot=${basepc}
    export ROBOT_VERSION="steel"
    #Simulation map/world
    export NAVIGATION_MAP="${PATH_TO_MAPS}/clf_map.yaml"
    export SIMULATION_WORLD="clf"

else
    echo "### SIMMODE IS NOT ACTIVE ###"
    export prefix="/vol/tiago/kinetic-clf/"
    export laptop=${basepc}
    export robot=tiago-47c

    #Real map
    export NAVIGATION_MAP="${PATH_TO_MAPS}/central_lab.yaml"

    export alsa_device="plug_tiago_mono"
fi

#source ./paths.sh
export PATH="${prefix}/bin:$PATH"

#ROS source alias
export setup_suffix=$(echo $SHELL | cut -d "/" -f3-)
alias source_ros="source ${prefix}/setup.${setup_suffix}"

#Map/World paths
export PATH_TO_MAPS="${prefix}/share/tiago_clf_nav/data"

#PocketSphinx paths
export PATH_TO_PSA_CONFIG="${prefix}/share/SpeechRec/psConfig"

# Path to bonsai config
export PATH_TO_BONSAI_ROBOCUP_CONFIG="${prefix}/opt/bonsai_robocup_addons/etc/bonsai_configs"
export PATH_TO_BONSAI_ROBOCUPTASKS_CONFIG="${prefix}/opt/bonsai_robocup_exercise/etc/bonsai_configs"
export PATH_TO_BONSAI_TIAGO_CONFIG="${prefix}/opt/bonsai_tiago_addons/etc/bonsai_configs"

# Path to scxml locations
export PATH_TO_BONSAI_ROBOCUP_SCXML="${prefix}/opt/bonsai_robocup_addons/etc/state_machines"
export PATH_TO_BONSAI_CORE_SCXML="${prefix}/opt/bonsai-scxml_engine/etc/behaviors"
export PATH_TO_BONSAI_ROBOCUPTASKS_SCXML="${prefix}/opt/bonsai_robocup_exercise/etc/state_machines"
export PATH_TO_BONSAI_PEPPER_SCXML="${prefix}/opt/bonsai2-pepper-dist/etc/state_machines"
export PATH_TO_BONSAI_TIAGO_SCXML="${prefix}/opt/bonsai_tiago_addons/etc/state_machines"

# Create mapping variable, used by bonsai2 to resolve "src=" attributes in scxml files
export BONSAI_MAPPING="ROBOCUP=${PATH_TO_BONSAI_ROBOCUP_SCXML} SCXML=${PATH_TO_BONSAI_CORE_SCXML} EXERCISE=${PATH_TO_BONSAI_ROBOCUPTASKS_SCXML} PEPPER=${PATH_TO_BONSAI_PEPPER_SCXML} TIAGO=${PATH_TO_BONSAI_TIAGO_SCXML}"

##########

#Laptop setup
export ROS_MASTER_URI=http://${robot}:11311

#Bonsai Task
export PATH_TO_BONSAI_CONFIG="${PATH_TO_BONSAI_TIAGO_CONFIG}/DefaultTiagoConfig.xml"
export PATH_TO_BONSAI_TASK="${PATH_TO_BONSAI_TIAGO_SCXML}/debug/test.xml"

#pocketsphinx_grammars
export PSA_CONFIG="${PATH_TO_PSA_CONFIG}/tasks/inspection.conf"

# Rviz config
export RVIZ_CONFIG="${prefix}/share/tobi_sim/config/tiago.rviz"

# camera topics
#if [[ -z "$RGB_CAM_TOPIC" ]]; then
#    export RGB_CAM_TOPIC=/xtion/rgb
#fi
#if [[ -z "$DEPTH_CAM_TOPIC" ]]; then
#    export DEPTH_CAM_TOPIC=/xtion/depth_registered
#fi

# segmentation
export SEGMENTATION_CONFIGS="${prefix}/share/clf_object_recognition_config/config"
export KINECT_COLOR_XML="${SEGMENTATION_CONFIGS}/kinect-color-vga.xml"
export KINECT_DEPTH_XML="${SEGMENTATION_CONFIGS}/kinect-depth-vga.xml"
export SEGMENTER_PATH="${SEGMENTATION_CONFIGS}/segmentation_config.xml"
export ROBOT_FILTER="${SEGMENTATION_CONFIGS}/robotfilter.xml"
export OBJECT_MERGER_THRESHOLD=0.5

# Default KBASE locations
export PATH_TO_MONGOD_CONFIG="$prefix/share/robocup_data/mongod/mongod.conf"
export PATH_TO_KBASE_CONFIG="$prefix/share/robocup_data/knowledgeBase/configs/use_challenge_db.yaml"
export PATH_TO_EDIT_KBASE_CONFIG="$prefix/share/robocup_data/knowledgeBase/configs/edit_challenge_db.yaml"

#object
export PATH_TO_CLASSIFIERS="${prefix}/share/storing_groceries_node/object"
export OBJECT_DET_GRAPH="${PATH_TO_CLASSIFIERS}/detection/frozen_inference_graph.pb"
export OBJECT_DET_LABELS="${PATH_TO_CLASSIFIERS}/detection/label_map.pbtxt"
export OBJECT_REC_PATH="${PATH_TO_CLASSIFIERS}/recognition"
export OBJECT_REC_GRAPH="${OBJECT_REC_PATH}/output_graph.pb"
export OBJECT_REC_LABELS="${OBJECT_REC_PATH}/output_labels.txt"
export OBJECT_DETECTION_THRESHOLD="0.5"

export GRASPING_OBJECTS_CONFIG="${prefix}/share/storing_groceries_node/config/challenge2020.yaml"

# Other
export ACTOR_VEL_TOPIC="/Olf/cmd_vel"
